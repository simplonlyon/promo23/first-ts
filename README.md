# first-ts

## How To use
1. Cloner le projet
2. Exécuter `npm i` pour installer les dépendances
3. Exécuter `npm start` pour lancer le projet
4. Accéder au projet sur http://localhost:1234
5. Vous pouvez lancer le navigateur en mode débug en allant dans l'onglet Exécuter et debugger et en choisissant "lancer avec firefox" ou "lancer avec chrome" et en faisant le bouton lecture ou F5 (sur pc/linux)

## Structure d'un projet
En général, les projets de développement auront une structure comme on l'a fait pour celui ci avec :
à la racine :
* **package.json** => fichier qui contient toutes les informations du projet (sa version, les scripts de lancement, la liste de ses dépendances/librairies)
* **README.md** => qui contiendra en markdow une présentation du projet
* **.gitignore** => pour indiquer les dossiers/fichiers qu'on souhaite que git ne prenne pas en compte (en général on aura le dossier node_modules, dist, et d'autres)
* **node_module** => dossier contenant tous les fichiers des dépendances du projet qu'on a installé avec npm
* **dist** => dossier contient le résultat du build du projet (c'est généralement le contenu de ce dossier qu'on mettra en ligne au moment du déploiement)
* **src** => dossier contenant les sources, ce que sont les sources va dépendre un peu selon les projets, mais en JS/TS, on aura les fichier typescript
* **public** => dossier contenant les fichiers static, comme le html, le favicon, potentiellement le css et les graphismes

## Exercices

### Premières conditions ([ts](src/exo-condition.ts))
1. Créer un nouveau fichier exo-condition.html et un fichier exo-condition.ts et les lier ensemble (avec parcel quand on crée un nouveau html, il faut arrêter le npm start avec ctrl+c et le relancer)
2. Dans le fichier ts, créer 2 variable, age et isOfAge et faire une condition qui va faire que si age est supérieur ou égal à 18, alors on assigne true à isOfAge
3. En dessous, faire une autre condition qui va vérifier si isOfAge est true, et si oui, on affiche un message avec un console.log genre 'Welcome to the tax paying app' et sinon on affiche 'Go home kiddo' 

### Condition calcul ([ts](src/exo-condition.ts))
1. A la suite du fichier exo-condition.ts, créer 2 nouvelles variables a et b qui contiendront n'importe quoi comme nombre (genre 3 et 5)
2. Créer une troisième variable operator qui contiendra une chaîne de caractère
3. Faire plusieurs conditions qui vont vérifier : si operator vaut '+' alors on additionne a et b et on affiche le résultat, si ça contient '-' soustraction, '/' division et '*' multiplication
4. Si on a aucun des opérateurs possibles, on affiche un message 'incorrect operator' avec un console.log
5. à la place de mettre directement une valeur dans operator, faire en sorte de lui assigner un prompt('choose an operator') qui affichera un popup permettant à l'utilisateur·ice d'entrer un opérateur directement 

## Boucle choix nombre de tours ([ts](src/exo-loop.ts))
1. Dans un fichier exo-loop.ts (lié à un exo-loop.html). Créer une variable loopNumber et mettre dedans le résultat d'un prompt (converti en Number)
2. Créer une boucle for ou while, comme vous voulez, qui va faire le nombre de tours indiqué par loopNumber
3. Dans cette boucle, faire un console log de coucou ou autre, osef

## Boucler sur les lettres d'un prénom ([ts](src/exo-loop.ts))
1. Dans le même fichier (ou un autre peu importe), créer une variable name qui va contenir un prompt
2. Faire un console.log du nombre de lettre qu'il y a dans ce prénom
3. Faire une boucle qui fera autant de tour qu'il y a de lettre dans le prénom 
4. A chaque tour de boucle, essayer d'afficher quelle lettre du prénom se trouve à la position actuelle de la boucle


## Pyramid d'étoiles ([ts](src/exo-pyramid.ts))
Objectif final, avoir ça via des boucles : 
```
    *
   ***
  *****
 *******
*********
```
1. Créer un nouveau point d'entrée exo-pyramid.html / exo-pyramid.ts
2. Pour commencer, on peut essayer de faire une première boucle pour avoir autant de console log qu'on a d'étage à la pyramide, donc ici 5 (peu importe ce qu'on console log)
3. Ensuite, on peut essayer de trouver un calcul très savant pour afficher à chaque tour de boucle le nombre d'étoile par étage de la pyramide, donc genre ça comme résultat :
```
1
3
5
7
9
```
4. Une fois qu'on a le bon nombre d'étage et une variable qui contient le nombre d'étoile à afficher par étage, "ya pu qu'à" faire en sorte d'afficher les étoiles en questions (par exemple avec une boucle imbriquée qui concatène dans une phrase, comme à l'exo d'avant)
Résultat :
```
*
***
*****
*******
*********
```
5. Ensuite on recommence un peu la même chose qu'à l'étape 3, mais cette fois ci pour le nombre d'espace avant chaque étoile pour avoir un truc qui ressemble à ça :
```
4*
3***
2*****
1*******
0*********
```
6. Enfin, on fait comme à l'étape 4 pour concaténer les espaces avant les étoiles, et on obtient notre pyramide
   
Bonus : Pouvoir changer le nombre d'étage avec une variable et pourquoi pas aussi le caractère (pour pouvoir faire une pyramide de coeurs par exemple <3)