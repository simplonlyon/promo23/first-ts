let age = 13;
let isOfAge = false;

//let isOfAge = age >= 18; //on peut se passer de la première condition en assignant directement le résultat de l'opérateur logique à la variable


if (age >= 18) {
    isOfAge = true;
}

if (isOfAge) {
    console.log('Welcome to the tax paying app');
} else {
    console.log('Go home kiddo');
}

let a = 3;
let b = 5;
let operator = prompt('Enter an operator'); // Le prompt est un truc du JS qu'on utilisera pas souvent après la première semaine mais qui permet de demander un input utilisateur·ice directement
let result = 0;



if (operator == '+') {
    result = a + b;
} else if (operator == '-') {
    result = a - b;
} else if (operator == '*') {
    result = a * b;
} else if (operator == '/') {
    result = a / b;
} else {
    console.log('Invalid operator');
    // throw new Error('Invalid operator'); // Si on veut on peut aussi déclencher une erreur pour stopper le code
}

console.log('The result of ' + a + operator + b + ' is ' + result);
// console.log(`The result of ${a}${operator}${b} is ${result}`);



//version avec un switch
// switch (operator) {
//     case '+':
//         result = a + b;
//         break;
//     case '-':
//         result = a - b;
//         break;
//     case '*':
//         result = a * b;
//         break;
//     case '/':
//         result = a / b;
//         break;
//     default:
//        console.log('Invalid operator');
//        break;
// }