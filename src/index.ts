let word: string = 'Salut';

let age: number = 20;
let isOfAge: boolean = true; //ou false

//Le any, à utiliser le moins possible
let nimportequoi: any = 'Du texte';
nimportequoi = true;
nimportequoi = 10;

let notExist: undefined = undefined;
let noValue: null = null;
console.log(age);

if (age == 20 && isOfAge) {

}

/**
 * true && true == true
 * true && false == false
 * false && true == false
 * false && false == false
 * 
 * 
 * true || true == true
 * true || false == true
 * false || true == true
 * false || false == false
 */

//Exemples de boucle
let counter = 0;
while (counter < 10) {
    console.log(counter);
    // counter = counter + 1;
    // counter += 1;
    counter++;
}

for (let counter = 0; counter < 10; counter++) {

    console.log(counter);
}


//exemple de tableau
let firstArray = ['Ga', 'Zo', 'Bu', 'meu'];


for (let i = 0; i < firstArray.length; i++) {
    console.log(firstArray[i]);

}

for (const item of firstArray) {
    console.log(item);
}